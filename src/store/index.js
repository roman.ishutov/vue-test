import { createStore } from "vuex";
import post from "./modules/post";
import auth from "./modules/auth";

const store = createStore({
  state: [],
  mutations: {},
  actions: {},
  getters: {},
  modules: { post, auth },
});
export default store;
