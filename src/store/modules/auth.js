export default {
  state: {
    login: '',
  },
  mutations: {
    updateUserData(state, data) {
      state.login = data;
    },
  },
  actions: {
    setUserData(ctx, data) {
      ctx.commit('updateUserData', data);
    },
  },
  getters: {
    userData(state) {
      return state.login;
    },
  },
};
