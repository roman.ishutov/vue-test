export default {
  state: {
    posts: [],
  },
  mutations: {
    updatePosts(state, posts) {
      state.posts = posts;
    },
    deleteOnePost(state, id) {
      state.posts = state.posts.filter((post) => post.id !== id);
    },
    createOnePost(state, newPost) {
      state.posts.push(newPost);
    },
  },
  actions: {
    async fetchPosts(ctx) {
      const res = await fetch(
        'https://jsonplaceholder.typicode.com/posts?_limit=5'
      );
      const posts = await res.json();
      ctx.commit('updatePosts', posts);
    },
    deletePost(ctx, id) {
      ctx.commit('deleteOnePost', id);
    },
    createPost(ctx, newPost) {
      ctx.commit('createOnePost', newPost);
    },
  },
  getters: {
    allPosts(state) {
      return state.posts;
    },
  },
};
