import { createRouter, createWebHistory } from 'vue-router';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('../components/Home.vue'),
    },
    {
      path: '/todos',
      name: 'todos',
      component: () => import('../components/Todos.vue'),
    },

    {
      path: '/auth',
      name: 'auth',
      component: () => import('../components/Auth.vue'),
    },
  ],
});

export default router;
